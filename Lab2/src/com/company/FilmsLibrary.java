package com.company;

import java.util.*;

public class FilmsLibrary {
    private List<Film> films;
    private Map<Integer, Set<String>> yearsAndNamesOfFilms;
    private Map<String, List<Film>> producersAndFilms;

    public  FilmsLibrary()
    {
        films = new ArrayList<>();
        yearsAndNamesOfFilms = new HashMap<>();
        producersAndFilms = new HashMap<>();
    }
    public void addFilm(Film film)
    {
        films.add(film);
    }

    private void formYearsAndNamesOfFilms()
    {
        yearsAndNamesOfFilms.clear();
        List<Integer> years = new ArrayList<>();
        for (Film f: films) {
            if( years.contains(f.getYearOfIssue()))
                continue;
            else
                years.add(f.getYearOfIssue());
        }
        for( int i=0; i< years.size(); i++) {
            int year = years.get(i);
            Set<String> currentSetOfNameOfFilms = new HashSet<>();
            for (Film f : films) {
                if (f.getYearOfIssue() == year) {
                    currentSetOfNameOfFilms.add(f.getNameOfFilm());
                } else
                    continue;
            }
            yearsAndNamesOfFilms.put(year, currentSetOfNameOfFilms);
        }
    }

    public void showFilmsByYears()
    {
        this.formYearsAndNamesOfFilms();
        Set<Integer> keys = yearsAndNamesOfFilms.keySet();
        Object [] keysArr = keys.toArray();
        for( int i=0; i<keysArr.length; i++)
        {
            System.out.println("Films made in " + keysArr[i]);
            Set<String> current = yearsAndNamesOfFilms.get(keysArr[i]);
            Iterator iterator = current.iterator();
            while (iterator.hasNext()) {
                System.out.println("   " + iterator.next());
            }
            System.out.println();
        }
    }

    private void formProducersAndFilms()
    {
        producersAndFilms.clear();
        List<String> producers = new ArrayList<>();
        for (Film f: films) {
            for (String s: f.getListOfProducers()) {
                if( producers.contains(s))
                    continue;
                else
                    producers.add(s);
            }
        }
        for( int i=0; i< producers.size(); i++) {
            String producer = producers.get(i);
            List<Film> currentFilms = new ArrayList<>();
            for(Film f: films)
            {
                List<String> namesOfProducers = f.getListOfProducers();
                for(int j=0;j<namesOfProducers.size(); j++) {
                    if( namesOfProducers.get(j).equals(producer) )
                    {
                        currentFilms.add(f);
                        break;
                    }
                }
            }
            producersAndFilms.put(producer, currentFilms);
        }
    }

    public void showFilmsByProducers()
    {
        this.formProducersAndFilms();
        Set<String> keys = producersAndFilms.keySet();
        Object [] keysArr = keys.toArray();
        for( int i=0; i<keysArr.length; i++)
        {
            System.out.println("Films made by " + keysArr[i]);
            List<Film> current = producersAndFilms.get(keysArr[i]);
            Iterator iterator = current.iterator();
            while (iterator.hasNext()) {
                System.out.println("   " + iterator.next());
            }
            System.out.println();
        }
    }

    public void fillLibrary(List<String> strings)
    {
        for (int i = 0; i < strings.size(); i++) {
            String[] words;
            words = strings.get(i).split(" ");
            Film film = new Film(words[0], Integer.parseInt(words[1]), words[2]);
            for (int j = 3; j < words.length; j++)
                film.addProducer(words[j]);
            this.addFilm(film);
        }
    }
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("");
        for (Film film : films ) {
            result.append(film.toString() + "\n");
        }
        return result.toString();
    }
}
