package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        while (true) {
            System.out.println("Please, choose letter to do something");
            System.out.println("a - read data from first file, b - read data from second file\nAny other letter - exit");
            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();
            String path;
            if (choice.equals("a")) {
                path = "D:/info1.txt";
            } else if (choice.equals("b")) {
                path = "D:/info2.txt";
            } else {
                break;
            }
            FilmsLibrary filmsLibrary = new FilmsLibrary();
            filmsLibrary.fillLibrary(FileReaderHelper.readStringsFromFile(path));
            System.out.println("You have: ");
            System.out.println(filmsLibrary.toString());
            filmsLibrary.showFilmsByYears();
            System.out.println();
            filmsLibrary.showFilmsByProducers();
        }
    }
    public static class FileReaderHelper
    {
        public static List<String> readStringsFromFile(String path)
        {
            List<String> stringsFromFile = new ArrayList<>();
            try{
                File file = new File(path);
                FileReader fr = new FileReader(file);
                BufferedReader reader = new BufferedReader(fr);
                String line = reader.readLine();
                while (line != null) {
                    stringsFromFile.add(line);
                    line = reader.readLine();
                }
            }
            catch (IOException exception) {
                exception.printStackTrace();
            }
            return stringsFromFile;
        }
    }
}
