package com.company;

import java.util.ArrayList;
import java.util.List;

public class Film {
    private String nameOfFilm;
    private int yearOfIssue;
    private String nameOfDirector;
    private List<String> listOfProducers;

    public Film( String nameOfFilm, int yearOfIssue, String nameOfDirector )
    {
        this.nameOfFilm = nameOfFilm;
        this.yearOfIssue = yearOfIssue;
        this.nameOfDirector = nameOfDirector;
        listOfProducers = new ArrayList<>();
    }

    public void addProducer(String nameOfProducer)
    {
        listOfProducers.add(nameOfProducer);
    }

    public List<String> getListOfProducers() {
        return listOfProducers;
    }

    public String getNameOfDirector() {
        return nameOfDirector;
    }

    public int getYearOfIssue() {
        return yearOfIssue;
    }

    public String getNameOfFilm() {
        return nameOfFilm;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("");
        result.append("It is " + "\"" + nameOfFilm + "\" made in " + yearOfIssue + " by " + nameOfDirector + " and: ");
        for (String s : listOfProducers) {
            result.append(s + " ");
        }
        return result.toString();
    }
}
