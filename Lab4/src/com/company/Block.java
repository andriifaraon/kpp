package com.company;

public class Block extends Thread{
    int [][] matr;
    int n;
    int m;
    int start;
    int end;
    int numberOfLambda;
    String threadInfo;
    public String printTreadInfo(){

        StringBuilder sb = new StringBuilder();
        sb.append("Id: "+ Thread.currentThread().getId() + "\n");
        sb.append("Name: "+Thread.currentThread().getName() + "\n");
        sb.append("Priority: "+ Thread.currentThread().getPriority()+ "\n");
        sb.append("State: "+ Thread.currentThread().getState()+ "\n");
        sb.append("Is alive : "+Thread.currentThread().isAlive()+ "\n\n");
        return  sb.toString();

    }

    @Override
    public void run() {
        threadInfo = printTreadInfo();
        findOwnValues();
    }

    public Block(int [][] matr, int n, int m, int start, int end, int numberOfLambda)
    {
        this.n = n;
        this.m = m;
        this.start = start;
        this.end = end;
        this.numberOfLambda = numberOfLambda;
        this.matr = new int[n][m];
        for( int i =0; i<n; i++)
        {
            for( int j=0; j<m; j++)
            {
                this.matr[i][j] = matr[i][j];
            }
        }
    }
    public void findOwnValues()
    {
        String result;
        int footStep = matr[start][start] + matr[end][end];
        int determinant = determinant(start, end);
        double underSqrt = Math.pow(footStep,2) - 4*determinant;
        if( underSqrt < 0 )
        {
            result = "Lambda #" + numberOfLambda + " " + footStep/2 + "+j , " + footStep/2 + "-j";
        }
        else
        {
            double l1 = (footStep + Math.sqrt(underSqrt))/2;
            double l2 = (footStep - Math.sqrt(underSqrt))/2;
            result = "Lambda #" + numberOfLambda + " " + l1 + " , " + l2;
        }
        System.out.println(result);
    }
    public int determinant(int start, int end)
    {
        int firstMultiple = matr[start][start]*matr[end][end];
        int secondMultiple = matr[start][end]*matr[end][start];
        return  firstMultiple - secondMultiple;
    }
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("");
        for( int i=start; i<=end; i++)
        {
            for(int j=start; j<=end; j++)
            {
                stringBuilder.append(matr[i][j] + " ");
            }
            stringBuilder.append("\n");
        }
        stringBuilder.append("\n");
        return  stringBuilder.toString();
    }
}
