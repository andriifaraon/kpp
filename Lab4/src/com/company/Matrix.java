package com.company;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Matrix {
    int n;
    int m;
    int [][] matr;
    public Matrix(List<String> stringsFromFile)
    {
        n = stringsFromFile.size();
        m = (stringsFromFile.get(0)).split("\\s+").length;
        matr = new int[n][m];
        for (int i=0; i<stringsFromFile.size(); i++) {
            String [] nums = stringsFromFile.get(i).split("\\s+");
            for( int j=0; j<nums.length; j++)
            {
                matr[i][j] = Integer.parseInt(nums[j]);
            }
        }
    }
    public int getM() {
        return m;
    }
    public int getN() {
        return n;
    }
    public void findIndexesOfBlocks(JTextArea jTextArea) throws InterruptedException {
        List<Integer> indexes = new ArrayList<Integer>();
        for( int i=0; i<n; i+=2)
        {
            indexes.add(i);
        }
        if(n%2 == 0)
        {
            List<Block> threads = new ArrayList<Block>();
            for(int i=0; i<indexes.size(); i++)
            {
                Block b = new Block(matr, n, m, indexes.get(i), indexes.get(i)+1, i + 1);
                threads.add(b);
                b.start();
                Thread.sleep(100);
                jTextArea.append(b.threadInfo);
            }
        }
        else
        {
            List<Block> threads = new ArrayList<Block>();
            for(int i=0; i<indexes.size()-1; i++)
            {
                Block b = new Block(matr, n, m, indexes.get(i), indexes.get(i)+1, i + 1);
                threads.add(b);
                b.start();
                Thread.sleep(100);
                jTextArea.append(b.threadInfo);
            }
            System.out.println("Lambda #" + indexes.size() + " " + matr[n-1][m-1]);
        }
    }
    public void findIndexesOfBlocksWithExecutorService() throws InterruptedException {
        List<Integer> indexes = new ArrayList<Integer>();
        for( int i=0; i<n; i+=2)
        {
            indexes.add(i);
        }
        if(n%2 == 0)
        {
            ExecutorService executorService = Executors.newFixedThreadPool(indexes.size());
            for(int i=0; i<indexes.size(); i++)
            {
                Block b = new Block(matr, n, m, indexes.get(i), indexes.get(i)+1, i + 1);
                executorService.submit(b);
            }
        }
        else
        {
            ExecutorService executorService = Executors.newFixedThreadPool(indexes.size()-1);
            for(int i=0; i<indexes.size()-1; i++)
            {
                Block b = new Block(matr, n, m, indexes.get(i), indexes.get(i)+1, i + 1);
                executorService.submit(b);
            }
            System.out.println("Lambda #" + indexes.size() + " " + matr[n-1][m-1]);
        }
    }
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("");
        for( int i=0; i<n; i++)
        {
            for(int j=0; j<m; j++)
            {
                stringBuilder.append(matr[i][j] + " ");
            }
            stringBuilder.append("\n");
        }
        stringBuilder.append("\n");
        return  stringBuilder.toString();
    }
}
