package com.company;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Matrix matrix = new Matrix(FileReaderHelper.readStringsFromFile(("src/matrix_data")));
        JFrame f = new JFrame();
        f.setTitle("Gauss");
        f.setBounds(500, 250, 100, 300);
        JTextArea jTextArea = new JTextArea("  ");
        jTextArea.setBounds(10, 25, 500, 200);
        jTextArea.setSize(600, 600);
        f.add(jTextArea);
        f.setBackground(Color.WHITE);
        jTextArea.setText("Executing my threads\n");
        jTextArea.getScrollableTracksViewportHeight();
        f.setSize(600, 600);
        f.setLayout(null);
        f.setVisible(true);
        //matrix.findIndexesOfBlocksWithExecutorService();
        matrix.findIndexesOfBlocks(jTextArea);
    }
    public static class FileReaderHelper
    {
        public static List<String> readStringsFromFile(String path)
        {
            List<String> stringsFromFile = new ArrayList<>();
            try{
                File file = new File(path);
                FileReader fr = new FileReader(file);
                BufferedReader reader = new BufferedReader(fr);
                String line = reader.readLine();
                while (line != null) {
                    stringsFromFile.add(line);
                    line = reader.readLine();
                }
            }
            catch (IOException exception) {
                exception.printStackTrace();
            }
            return stringsFromFile;
        }
    }
}
