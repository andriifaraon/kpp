package com.company;

public class SpecialClothes extends Clothes{
    private SpecialClothesType specialClothesType;
    public SpecialClothes( String nameOfBrand, ClothesType clothesType, double price, int size,
                           SpecialClothesType specialClothesType )
    {
        super(nameOfBrand, clothesType, price, size);
        this.isCeremonial = false;
        this.specialClothesType = specialClothesType;
    }

    @Override
    public void makeDiscount(double percent) {
        price -= (price * (percent + 3)/100);
    }

    @Override
    public String toString() {
        return String.format("%s by %s, price: %f, size: %d for %s",
                clothesType.toString(), nameOfBrand, price, size, specialClothesType.toString() );
    }
}
