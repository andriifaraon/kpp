package com.company;

import java.util.*;

public class ClothesManager {
    private List<Clothes>  clothesAssortment;

    public ClothesManager()
    {
        clothesAssortment = new ArrayList<>();
    }
    public ClothesManager(List<Clothes> clothesAssortment)
    {
        this.clothesAssortment = clothesAssortment;
    }
    public void addItem( Clothes clothes )
    {
        clothesAssortment.add(clothes);
    }
    public void sortClothesAssortmentBySize(boolean typeOfSorting)
    {
        if(typeOfSorting)
            Collections.sort(clothesAssortment, new Clothes.ComparatorBySize());
        else
            Collections.sort(clothesAssortment, Collections.reverseOrder(new Clothes.ComparatorBySize()));
    }
    public void sortClothesAssortmentByPrice(boolean typeOfSorting )
    {
        if( clothesAssortment.size() > 0 )
        {
            if(typeOfSorting)
                Collections.sort(clothesAssortment, clothesAssortment.get(0).new ComparatorByPrice());
            else
                Collections.sort(clothesAssortment, Collections.reverseOrder(clothesAssortment.get(0).new ComparatorByPrice()));
        }
    }
    public void sortClothesAssortmentByNameOfBrand(boolean typeOfSorting)
    {
        Comparator<Clothes> comparatorByNameOfBrand = new Comparator<Clothes>() {
            @Override
            public int compare(Clothes o1, Clothes o2) {
                return o1.nameOfBrand.compareTo(o2.nameOfBrand);
            }
        };
        if(typeOfSorting)
            Collections.sort(clothesAssortment, comparatorByNameOfBrand);
        else
            Collections.sort(clothesAssortment, Collections.reverseOrder(comparatorByNameOfBrand));
    }
    public void sortClothesAssortmentByCeremonyPossibility(boolean typeOfSorting)
    {
        if(typeOfSorting)
            Collections.sort(clothesAssortment, (o1,o2) -> Boolean.compare(o1.isCeremonial, o2.isCeremonial));
        else
            Collections.sort(clothesAssortment, Collections.reverseOrder((o1,o2) -> Boolean.compare(o1.isCeremonial, o2.isCeremonial)));
    }
    public ClothesManager findByNameOfBrand( String nameOfBrand )
    {
        List<Clothes> resultList = new ArrayList<>();
        for( int i=0; i<clothesAssortment.size(); i++ )
        {
            if( clothesAssortment.get(i).getNameOfBrand().compareTo(nameOfBrand) == 0)
                resultList.add(clothesAssortment.get(i));
        }
        return new ClothesManager(resultList);
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Clothes c: clothesAssortment ) {
            sb.append(c.toString()+"\n");
        }
        return sb.toString();
    }

}
