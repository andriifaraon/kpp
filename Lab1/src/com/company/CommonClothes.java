package com.company;

public class CommonClothes extends Clothes{
    public CommonClothes( String nameOfBrand, ClothesType clothesType, double price, int size,
                           boolean isCeremonial )
    {
        super(nameOfBrand, clothesType, price, size);
        this.isCeremonial = isCeremonial;
    }
    @Override
    public void makeDiscount(double percent) {
        price -= (price * percent / 100);
    }

    @Override
    public String toString() {
        String res = "";
        if(isCeremonial)
            res = String.format("%s by %s, price: %f, size: %d for ceremony", clothesType.toString(), nameOfBrand, price, size );
        else
            res = String.format("%s by %s, price: %f, size: %d", clothesType.toString(), nameOfBrand, price, size );
        return res;
    }
}
