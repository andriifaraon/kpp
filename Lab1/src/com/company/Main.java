package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        ClothesManager manager = new ClothesManager();
        manager.addItem(new CommonClothes("Zara", ClothesType.Dress, 1200, 34, true));
        manager.addItem(new SpecialClothes("Mango", ClothesType.Jacket, 1400, 28, SpecialClothesType.Guard ));
        manager.addItem(new CommonClothes("Zara", ClothesType.Suit, 1200, 54, true));
        manager.addItem(new SpecialClothes("Gucci", ClothesType.Shirt, 14000, 22, SpecialClothesType.Doctor ));
        manager.addItem(new CommonClothes("Pull&Bear", ClothesType.Trousers, 900, 37, false));
        manager.addItem(new SpecialClothes("Reserved", ClothesType.Jacket, 2400, 28, SpecialClothesType.Policeman ));

        manager.sortClothesAssortmentBySize(false);
        System.out.println(manager.toString());
        System.out.println();

        manager.sortClothesAssortmentByPrice(true);
        System.out.println(manager.toString());
        System.out.println();

        manager.sortClothesAssortmentByNameOfBrand(true);
        System.out.println(manager.toString());
        System.out.println();

        manager.sortClothesAssortmentByCeremonyPossibility(true);
        System.out.println(manager.toString());
        System.out.println();

        ClothesManager managerForZara = manager.findByNameOfBrand("Zara");
        System.out.println(managerForZara.toString());
    }
}
