package com.company;

import java.util.Comparator;

public abstract class Clothes {
    protected String nameOfBrand;
    protected ClothesType clothesType;
    protected double price;
    protected int size;
    protected boolean isCeremonial;

    public Clothes( String nameOfBrand, ClothesType clothesType, double price, int size )
    {
        this.nameOfBrand = nameOfBrand;
        this.clothesType = clothesType;
        this.price = price;
        this.size = size;
    }
    public abstract void makeDiscount( double percent );
    public static class ComparatorBySize implements Comparator<Clothes>
    {
        @Override
        public int compare(Clothes o1, Clothes o2) {
            return Integer.compare(o1.size, o2.size);
        }
    }
    public class ComparatorByPrice implements Comparator<Clothes>
    {
        @Override
        public int compare(Clothes o1, Clothes o2) {
            return Double.compare(o1.price, o2.price);
        }
    }

    public String getNameOfBrand() {
        return nameOfBrand;
    }
}

