package com.company;

public enum ClothesType {
    Trousers,
    Shorts,
    Skirt,
    Dress,
    Suit,
    Jacket,
    Sweater,
    Shirt
}
