package com.company;

import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TextHelperTest {

    @org.junit.jupiter.api.Test
    void findLongestWordIndex() {
        //Arrange
        String [] mas = { "Andre", "maven", "Organization"};
        List<String> list = new ArrayList<String>(Arrays.asList(mas));
        TextHelper textHelper = new TextHelper("");
        int expected = 2;

        //Act
        int actual = textHelper.findLongestWordIndex(list);

        //Assert
        Assertions.assertEquals(expected, actual);
    }
    @org.junit.jupiter.api.Test
    void findVowelWordIndex() {
        //Arrange
        String [] mas = { "Taras", "Sentence" , "Andre", "maven", "Organization"};
        List<String> words = new ArrayList<String>(Arrays.asList(mas));
        TextHelper textHelper = new TextHelper("");
        int expected = 2;

        //Act
        int actual = textHelper.findFirstVowelWordIndex(words);

        //Assert
        Assertions.assertEquals(expected, actual);
    }
    @org.junit.jupiter.api.Test
    void findVowelWordIndexNotVowel() {
        //Arrange
        String [] mas = { "Taras", "Sentence", "maven"};
        List<String> wordsNotVowels = new ArrayList<String>(Arrays.asList(mas));
        TextHelper textHelper = new TextHelper("");
        int expected = -1;

        //Act
        int actual = textHelper.findFirstVowelWordIndex(wordsNotVowels);

        //Assert
        Assertions.assertEquals(expected, actual);
    }
}