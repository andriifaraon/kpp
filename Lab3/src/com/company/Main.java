package com.company;

import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException, ParseException {
        System.out.println("    TASK #1");
        DateHelper dateHelper = new DateHelper(FileReader.ReadFileInOneString("src/kpp_lab3eng"));
        System.out.println(dateHelper.toString());
        dateHelper.showDates();
        dateHelper.showResultString();

        System.out.println("\n    TASK #2");
        Scanner consoleScanner = new Scanner(System.in);
        TextHelper textHelper = new TextHelper(consoleScanner.nextLine());
        textHelper.showOldAndModify();
    }
    public static class FileReader
    {
        public static String ReadFileInOneString(String fileName) throws IOException {
            Scanner scanner = new Scanner(Path.of(fileName));
            StringBuilder stringBuilderFromFile = new StringBuilder("");
            scanner.useDelimiter(System.getProperty("line.separator"));
            while(scanner.hasNext()){
                stringBuilderFromFile.append(scanner.next());
            }
            scanner.close();
            return stringBuilderFromFile.toString();
        }
    }
}
