package com.company;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.lang.Character;

public class DateHelper {
    private String stringToOperate;
    private String resultString;
    private Map<Integer, String> stringsPositions;
    private List<Date> dates;
    public DateHelper( String stringToOperate ) {
        this.stringsPositions = new HashMap<Integer, String>();
        this.stringToOperate = stringToOperate;
        this.dates = new ArrayList<Date>();
    }

    public String getStringToOperate() {
        return stringToOperate;
    }

    private void findDates()
    {
        char [] masToOperate = stringToOperate.toCharArray();
        for (int i = 0; i < masToOperate.length; i++) {
            if(isDigit(masToOperate[i]))
            {
                if( i+10 <= masToOperate.length) {
                    if (isMatchFormat(stringToOperate.substring(i, i + 10), '-')) {
                        stringsPositions.put(i, stringToOperate.substring(i, i + 10));
                    }
                    if (isMatchFormat(stringToOperate.substring(i, i + 10), '.')) {
                        stringsPositions.put(i, stringToOperate.substring(i, i + 10));
                    }
                    if (isMatchFormat(stringToOperate.substring(i, i + 10), '/')) {
                        stringsPositions.put(i, stringToOperate.substring(i, i + 10));
                    }
                }
            }
        }
    }
    private void showRange() throws ParseException {
        List<String> stringDates = new ArrayList<String>();
        stringDates.addAll(stringsPositions.values());
        for (String s: stringDates) {
            dates.add(dateRecognizer(s));
        }
        Collections.sort(dates);
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        if( !dates.isEmpty())
            System.out.println("Range is: " + formatter.format(dates.get(0))
                    + " to " +  formatter.format(dates.get(dates.size()-1)));
    }
    private Date dateRecognizer(String s) throws ParseException {
        if( s.contains("-"))
            return new SimpleDateFormat("MM-dd-yyyy").parse(s);
        if( s.contains("."))
            return new SimpleDateFormat("dd.MM.yyyy").parse(s);
        if( s.contains("/"))
            return new SimpleDateFormat("dd/MM/yyyy").parse(s);
        return null;
    }
    public boolean isMatchFormat(String string, char divider)
    {
        char [] mas = string.toCharArray();
        if( mas.length != 10 )
            return false;
        if(isDigitOnNormalPlace(mas) && mas[2] == divider && mas[5] == divider )
            return  true;
        return false;
    }
    public boolean isDigitOnNormalPlace( char [] mas  )
    {
        if(isDigit(mas[0]) && isDigit(mas[1]) && isDigit(mas[3]) && isDigit(mas[4]))
        {
            if(isDigit(mas[6]) && isDigit(mas[7]) && isDigit(mas[8]) && isDigit(mas[9]))
            {
                return  true;
            }
        }
        return false;
    }
    public boolean isDigit( char ch )
    {
        if( Character.isDigit(ch))
            return true;
        return false;
    }
    public void showDates() throws ParseException {
        findDates();
        stringsPositions.forEach((i, s) -> System.out.println(s + " at position: " + i.toString()));
        showRange();
    }
    public void showResultString() throws ParseException {
        Calendar c = Calendar.getInstance();
        StringBuilder stringBuilderToOperate = new StringBuilder(stringToOperate);
        Set<Integer> datesPositions = stringsPositions.keySet();
        List<Integer> curr  = new ArrayList<>(datesPositions);
        Collections.sort(curr);
        for (Integer i: curr) {
            String currentString = stringToOperate.substring(i, i+10);
            Date currentDate = dateRecognizer(currentString);
            c.setTime(currentDate);
            c.add(Calendar.DAY_OF_MONTH, 1);
            String result = convertDateToString(currentString, currentDate);
            String day = "("+c.get(Calendar.DAY_OF_WEEK)+")";
            int new_index = i + curr.indexOf(i)*3;
            stringBuilderToOperate.delete( new_index, new_index+10);
            stringBuilderToOperate.insert(new_index, result + day);
        }
        resultString = stringBuilderToOperate.toString();
        System.out.println("Result string is: " + resultString);
    }
    public String convertDateToString(String string, Date date)
    {
        if( string.contains("-"))
        {
            DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
            return dateFormat.format(date);
        }
        if( string.contains("."))
        {
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            return dateFormat.format(date);
        }
        if( string.contains("/"))
        {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            return dateFormat.format(date);
        }
        return "";
    }
    @Override
    public String toString() {
        return "We work with such string: " + stringToOperate;
    }
}
